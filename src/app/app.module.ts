import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CacheLocalStorageModule, CacheModule, MemoryCacheModule } from '@dagonmetric/ng-cache';
import { ConfigModule } from '@dagonmetric/ng-config';
import { HttpConfigLoaderModule } from '@dagonmetric/ng-config/http-loader';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,

        ConfigModule.init(),
        HttpConfigLoaderModule,

        CacheModule,
        MemoryCacheModule,
        CacheLocalStorageModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
